import itertools

import pytest

from event_sourcing import (
    ScoringSystem,
    consume_game_events,
    get_event_iterator,
    parse_events,
)


def test_event_iterator():
    event_iterator = get_event_iterator()
    assert next(event_iterator) == {"team": "Federation", "player": "Kira", "points": 3}


def test_parse_events():
    event_iterator = get_event_iterator()
    message = next(parse_events(event_iterator))
    assert message.team == "Federation"
    assert message.player == "Kira"
    assert message.points == 3


def test_parse_two_events():
    event_iterator = get_event_iterator()
    game_events = parse_events(event_iterator)

    message = next(game_events)
    assert message.player == "Kira"

    message = next(game_events)
    assert message.player == "Worf"
    assert message.points == 3


def test_scoring_system():
    ss = ScoringSystem()
    ss.score_points("FakeTeam", 1)
    assert ss.get_current_score("FakeTeam") == 1


def test_scoring_system_with_multiple_items():
    ss = ScoringSystem()
    assert ss.get_current_score("FakeTeam") == 0
    ss.score_points("FakeTeam", 5)
    assert ss.get_current_score("FakeTeam") == 5
    ss.score_points("FakeTeam", 1)
    assert ss.get_current_score("FakeTeam") == 6


@pytest.mark.parametrize(
    "length, fed_score, space_score",
    [
        (4, 6, 4),
        (8, 9, 8),
        (1000, 85, 100),
    ],
)
def test_consume_game_events(length, fed_score, space_score):
    event_iterator = get_event_iterator()
    four_events = itertools.islice(parse_events(event_iterator), 0, length)
    scoring_system = ScoringSystem()
    consume_game_events(four_events, scoring_system)
    assert scoring_system.get_current_score("Federation") == fed_score
    assert scoring_system.get_current_score("Space Fantasy") == space_score
