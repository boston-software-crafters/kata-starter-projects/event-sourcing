import itertools
import json
from collections import defaultdict
from dataclasses import dataclass


@dataclass
class GameEventMessage:
    team: str
    player: str
    points: int


def get_event_iterator():
    with open("data.json", "r") as f:
        score_event_iterator = json.loads(f.read())
        yield from score_event_iterator


def parse_events(event_iterator):
    for event in event_iterator:
        yield GameEventMessage(
            team=event["team"], player=event["player"], points=event["points"]
        )


"""
Create methods for scoring points and retrieving the current score for a team:

* scorePoints(team, points)

* getCurrentScore(team)

Each time points are scored, your implementation should store the event data
(which team & how many points) into some kind of in-memory data structure
(such as a list or array).

The current score should be calculated by iterating the data structure rather
than storing a single integer value.
"""


class ScoringSystem:
    """
    Object that contains and displays scoring information
    """

    def __init__(self):
        self.scores_by_team = defaultdict(int)

    def score_points(self, team, points):
        # Process an event and store team/point data (somehow)
        self.scores_by_team[team] += points

    def get_current_score(self, team):
        # return the current score for the given team
        return self.scores_by_team[team]


def consume_game_events(events, scoring_system):
    # Iterates events and feeds them into the scoring system
    for event in events:
        scoring_system.score_points(event.team, event.points)
